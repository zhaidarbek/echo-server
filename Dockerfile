FROM registry.access.redhat.com/ubi8/openjdk-8-runtime

COPY target/echo-server-1.0-SNAPSHOT-jar-with-dependencies.jar /home/jboss/echo-server.jar

EXPOSE 8080
WORKDIR /home/jboss
CMD ["/usr/bin/java", "-jar", "echo-server.jar"]