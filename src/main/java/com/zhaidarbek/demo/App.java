package com.zhaidarbek.demo;

import io.undertow.Undertow;
import io.undertow.util.Headers;

/**
 * Hello world!
 */
public class App {
    private static final String serverResponse = "<html>" +
            "<body>" +
            "<h1>Hello, world!</h1>" +
            "</body>" +
            "</html>";

    public static void main(String[] args) {
        Undertow server = Undertow.builder()
                .addHttpListener(8080, "0.0.0.0")
                .setHandler(exchange -> {
                    exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/html");
                    exchange.getResponseSender().send(serverResponse);
                }).build();
        server.start();
    }
}
